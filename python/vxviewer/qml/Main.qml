import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import io.thp.pyotherside 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'vxviewer.localtoast'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('VxViewer')
        }

        Label {
            anchors.centerIn: parent
            text: i18n.tr('Hello World!')
        }
    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('vx_viewer', function() {
                console.log('vx_viewer module imported');
                python.call('vx_viewer.speak', ['Hello World!'], function(returnValue) {
                    console.log('vx_viewer.speak returned ' + returnValue);
                })
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
