import zmq
import numpy as np
import cv2
import math
import pickle

open("bh01.ipc", "w+")

#pickler = pickle.pickle

context = zmq.Context()
socket = context.socket(zmq.REP)
#socket.set(zmq.HEADER, "5")
#socket.set(zmq.FOOTER, "6")
#socket.setsockopt(zmq.ROUTER_RAW, 1)
socket.bind("ipc:///home/phil/Code/ProjectOhMartianStar/bh01.ipc")

while True:
	frameHolder = np.zeros((640, 480, 1), np.uint8)
	md = socket.recv_json()
	frame = socket.recv_pyobj()
	mess = pickle.loads(frame, encoding='latin1')
	#print(mess)
	#Receive data
	#frameReady = socket.recv_string()
	if mess.all():
		#Read the image now
		#frame = cv2.imread("bh01/img.png", -1)
		#print(frame)
		#frameHolder = frame.copy()
		#socket.send_string("FRAME RECIEVED")
		sweet = pickle.dumps("Mat-transported")
		socket.send_json(md, flags=zmq.SNDMORE)
		socket.send_pyobj(sweet)
		print("Mat transported!")
		cv2.imshow('Mat transporter', mess)
	else:
		print("Frame not ready")
		#socket.send_string("Frame not ready")
		message = pickle.dumps("BLANK FRAME")
		socket.send_json(md, flags=zmq.SNDMORE)
		socket.send_pyobj(message)
		#matZ = np.zeros((640, 480, 1), dtype= "uint8")
		#_, h, w = matZ.shape
		#frames = pickle.loads(frame)
		#mat = np.frombuffer(mess, dtype=md['dtype'])
		#print("Mat from buffer")
		#print(mat)
		#mat = mat.reshape(md['shape'])
		#print("Mat reshaped buffer")
		#print(mat)
		#mat = frame_convert2.pretty_depth_cv(mat)
		#print("Mat pretty buffer")
		#print(mat)
	if cv2.waitKey(10) == 27:
		cv2.destroyAllWindows()
		break

