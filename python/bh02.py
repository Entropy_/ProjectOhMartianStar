import time
import zmq

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("ipc:///home/phil/Code/ProjectOhMartianStar/bh01.ipc")

while True:
	socket.send(b"bh02 saying hi")
	time.sleep(1)
	message = socket.recv()
	print(message)
