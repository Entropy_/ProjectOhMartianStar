#include <string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <cmath>
#include <stb_image.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <shader/shader.h>
#include <camera/camera.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
/*
getting raw depth pixels, they're between 0 and 2047
float raw_depth_to_meters(int raw_depth)
{
  if (raw_depth < 2047)
  {
   return 1.0 / (raw_depth * -0.0030711016 + 3.3309495161);
  }
  return 0;
}


*/



void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 1600;
const unsigned int SCR_HEIGHT = 600;

//Init these a little bit apart to create a stereo image
Camera camBot(glm::vec3(0.0f, 0.0f, 3.0f));
Camera camBot02(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;


float deltaTime = 0.0f;	// Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame



GLuint depth_tex;

//Beginning on openNI
cv::VideoCapture capture(0);


int main()
{
// Declare depth map pieces
    cv::Mat depthMap;
//	capture.set(1, cv::CAP_PROP_OPENNI_REGISTRATION);
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "toots", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

	//Set global depth test
	glEnable(GL_DEPTH_TEST);


    // build and compile our shader program
    // ------------------------------------
	Shader bendShader("shaders/phil.vs", "shaders/phil.fs");
	Shader bendShader02("shaders/phil.vs", "shaders/phil.fs");
    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
float verticesPoke[640*480];
float vertices[]  = {
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

glm::vec3 cubePositions[] = {
  glm::vec3( 0.0f,  0.0f,  0.0f),
  glm::vec3( 2.0f,  5.0f, -15.0f),
  glm::vec3(-1.5f, -2.2f, -2.5f),
  glm::vec3(-3.8f, -2.0f, -12.3f),
  glm::vec3( 2.4f, -0.4f, -3.5f),
  glm::vec3(-1.7f,  3.0f, -7.5f),
  glm::vec3( 1.3f, -2.0f, -2.5f),
  glm::vec3( 1.5f,  2.0f, -2.5f),
  glm::vec3( 1.5f,  0.2f, -1.5f),
  glm::vec3(-1.3f,  1.0f, -1.5f)
};



	static std::vector<uint8_t> depth(640*480*4);

    unsigned int VAO, VBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // color attribute texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // load and create a texture 
    // -------------------------
    unsigned int texture1;
    // texture 1
    // ---------
    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1);
    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // load image, create texture and generate mipmaps
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 640, 480, 0, GL_RGB, GL_UNSIGNED_BYTE, &depth[0]);
        glGenerateMipmap(GL_TEXTURE_2D);


	bendShader.use();
	bendShader.setInt("texture1", 0);

	//grab da moose
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); 
    // render loop
    // -----------
	int countFrame = 1;
	GLuint text;
	cv::Mat image;
//	cap.initialize();
//	capture_stream.start();
	std::cout << image.channels() << std::endl;

    while (!glfwWindowShouldClose(window))
    {
//	capture_stream.readFrame(&image);
//	cv::Mat DepthImage(capture_stream_stats.getResolutionY(), capture_stream_stats.getResolutionX(), CV_8UC3, (void*)image.getData());
	capture.grab();
	cv::Point pt(0, 0);
	if (capture.grab()) {
		capture.retrieve(image);
		std::cout << image.at<float>(pt) << std::endl;

	}
//	capture >> image;
//	capture.read(image);
//	image.convertTo(image, CV_8UC3);
	cv::Size siz = image.size();

	glGenTextures(1, &text);
        glBindTexture( GL_TEXTURE_2D, text);

	//Set the wrapping and other variables
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, siz.width, siz.height, 0, GL_BGR, GL_UNSIGNED_BYTE, image.data);


	bool frame = false;
//        std::cout << "Average = " << x + y / 2 << std::endl;
	// Get the time
	float timeValue = glfwGetTime();

	float currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;  
        // input
        // -----
        processInput(window);
        // render
        // ------
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	bendShader.use();
	glViewport(0, 0, 800, 600);

	float radius = 10.0f;
	float camX = sin(glfwGetTime()) * radius;
	float camZ = cos(glfwGetTime()) * radius;
	glm::mat4 view(1);
	glm::mat4 view2(1);
	view = camBot.GetViewMatrix();
	view2 = camBot02.GetViewMatrix2();
	//view2 = glm::translate(view, glm::vec3(-1.0f, 0.0f, 0.0f));
        glm::mat4 projection(1);
        glm::mat4 projection2(1);
        projection = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        // pass transformation matrices to the shader
        projection2 = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        bendShader.setMat4("projection", projection);
        bendShader.setMat4("view", view);
	bendShader02.use();
        bendShader02.setMat4("projection", projection2);
        bendShader02.setMat4("view", view2);


        glBindVertexArray(VAO);
//SHADER ONE
	for (unsigned int i = 0;i < 10;i++) {
	        float bendValueFirst = (sin(timeValue+i) / 5.0f + 0.5f);
	      	bendShader02.setFloat4("ourColor", bendValueFirst, bendValueFirst, bendValueFirst, 1.0f);
	  glm::mat4 model2(1);
	  if (i == 0) {
		if (countFrame == 64) {
			countFrame = 1;
		} else {
			countFrame++;
		}
		model2 = glm::translate(model2, glm::vec3(0.0f, 0.0f, (0.010f)*-countFrame));
	  } else {
		  model2 = glm::translate(model2, cubePositions[i]);
	  }
	  float angle2 = (20.0f * i)*(timeValue);
	  model2 = glm::rotate(model2, glm::radians(angle2), glm::vec3(1.0f, 0.3f, 0.5f));
	  bendShader02.setMat4("model", model2);

	  glDrawArrays(GL_TRIANGLES, 0, 36);
	}

	glViewport(800, 0, 800, 600);
	bendShader.use();
	for (unsigned int i = 0;i < 10;i++) {
	        float bendValueFirst = (sin(timeValue+i) / 5.0f + 0.5f);
	      	bendShader.setFloat4("ourColor", bendValueFirst, bendValueFirst, bendValueFirst, 1.0f);
	  glm::mat4 model(1);
	  if (i == 0) {
		if (countFrame == 64) {
			countFrame = 1;
		} else {
			countFrame++;
		}
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, (0.010f)*-countFrame));
	  } else {
		  model = glm::translate(model, cubePositions[i]);
	  }
	  float angle = (20.0f * i)*(timeValue);
	  model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
	  bendShader.setMat4("model", model);

	  glDrawArrays(GL_TRIANGLES, 0, 36);
	}



        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &texture1);
	glDeleteTextures(1, &text);


    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}



// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
	
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	camBot.ProcessKeyboard(FORWARD, deltaTime);
	camBot02.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camBot.ProcessKeyboard(BACKWARD, deltaTime);
        camBot02.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camBot.ProcessKeyboard(LEFT, deltaTime);
        camBot02.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camBot.ProcessKeyboard(RIGHT, deltaTime);
        camBot02.ProcessKeyboard(RIGHT, deltaTime);

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset02 = xpos - lastX;
    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    camBot.ProcessMouseMovement(xoffset, yoffset);
    camBot02.ProcessMouseMovement(xoffset02, yoffset);
}
